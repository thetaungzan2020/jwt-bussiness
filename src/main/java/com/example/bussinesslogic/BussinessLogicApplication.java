package com.example.bussinesslogic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BussinessLogicApplication {

    public static void main(String[] args) {
        SpringApplication.run(BussinessLogicApplication.class, args);
    }

}
