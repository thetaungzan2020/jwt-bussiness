package com.example.bussinesslogic.filter;

import com.example.bussinesslogic.authentication.OtpAuthentication;
import com.example.bussinesslogic.authentication.UsernamePasswordAuthentication;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Component
public class InitialAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Value("${jwt.sign.key}")
    private String signkey;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String username=request.getHeader("username");
        String password=request.getHeader("password");

        String code=request.getHeader("code");
        if(code==null) {
            Authentication authentication=new UsernamePasswordAuthentication(username,password);
            authenticationManager.authenticate(authentication);
        }else {
            Authentication authentication=new OtpAuthentication(username,code);
            authenticationManager.authenticate(authentication);

            SecretKey key= Keys.hmacShaKeyFor(signkey.getBytes(StandardCharsets.UTF_8));
            String jwt= Jwts.builder()
                    .setClaims(Map.of("username",username))
                    .signWith(key)
                    .compact();
            response.setHeader("Authorization",jwt);
        }
        filterChain.doFilter(request,response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return !request.getServletPath().equals("/login");
    }
}
